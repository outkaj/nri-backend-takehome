#[macro_use] extern crate text_io;
extern crate csv;
extern crate rand;
use rand::{Rng};
use rand::distributions::{Weighted, WeightedChoice, IndependentSample};

fn main() {

  fn get_number_of_questions() -> i32 {
    println!("Hello! Please enter the number of questions you would like to answer today.");
    // get user input
    let mut amount: i32 = read!();
    // set boolean to track if condition is met
    let mut done = false;
    while !done {
        // if amount greater than 0, break out of loop
        if amount > 0 {
            println!("Thanks!");
            done = true;
        }
        else {
            // if amount 0 or less, provide error message. Program then re-prompts the user.
            println!("Invalid input. Number of questions must be greater than 0.");
            amount = read!();
        }
    }
    amount
 }

 // parse questions.csv into a vector of tuples corresponding to row data
 fn parse_csv(filepath: &str) -> Vec<(i32, String, i32, String, i32, f32)> {
   // create an empty vector that will hold the data
   let mut ids = vec![];
   // create a CSV reader
   let mut rdr = csv::Reader::from_file(filepath).unwrap();
   // parse CSV data and add to vector
   for record in rdr.decode() {
       let (strand_id, strand_name, standard_id, standard_name, question_id, difficulty):
       (i32, String, i32, String, i32, f32) = record.unwrap();
       ids.push((strand_id, strand_name, standard_id, standard_name, question_id, difficulty));
 }
   // return vector
   ids
}

//return list of question_ids as a vector
fn return_question_ids(amount: i32, ids: Vec<(i32, String, i32, String, i32, f32)>) -> Vec<i32> {
    // create a distribution for standard_ids with weights corresponding to their frequency
    let mut items = vec!(Weighted { weight: 2, item: 1 },
                    Weighted { weight: 1, item: 2 },
                    Weighted { weight: 3, item: 3 },
                    Weighted { weight: 2, item: 4 },
                    Weighted { weight: 3, item: 5 },
                    Weighted { weight: 1, item: 6 });
    let wc = WeightedChoice::new(&mut items);
    // create vector for strand_id values
    // as their frequency is equivalent, no need for a WeightedChoice
    let strand_ids = vec![1, 2];
    // create random number generator
    let mut rng = rand::thread_rng();
    // create empty vector that will hold the question_ids
    let mut question_ids: Vec<i32> = vec![];
    // create empty vector which will be used to store question ids before they are randomized
    let mut relevant_ids = vec![];
    // create a counter
    let mut counter = 0;
    // loop condition ensures we don't gather more questions than the user asked for
    while counter <= (amount - 1) as usize {
        for item in &ids {
            // choose a standard_id at random
            let standard_id = wc.ind_sample(&mut rng);
            if counter <=6 {
            // make sure corresponding strand_id is 1 and standard_id equals the random standard_id
                if item.0 == strand_ids[0] && item.2 == standard_id {
                    // add to the intermediary vector
                    relevant_ids.push(item.4);
                }
            else {
            // make sure corresponding strand_id is 2 and standard_id equals the random standard_id
                if item.0 == strand_ids[1] && item.2 == standard_id {
                    relevant_ids.push(item.4);
                }
            }
        }
    }
        // choose a question_id at random from the relevant_id vector
        let question_id = rand::thread_rng().choose(&relevant_ids).unwrap();
        // here I tried at one point to implement a condition to eliminate question_ids
        // that were already included, but this resulted in question lists that were too short
        question_ids.push(*question_id);
        // make sure counter increments before next iteration of loop
        counter = counter + 1;
    }
    question_ids
}

  let amount = get_number_of_questions();
  let ids = parse_csv("src/questions.csv");
  let question_ids = return_question_ids(amount, ids);
  println!("{:?}", question_ids);
}
