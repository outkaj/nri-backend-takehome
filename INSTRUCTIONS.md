You need the Rust language installed to run this program.

To install Rust on a Unix-based system, run the following command in your terminal:

```bash
$ curl https://sh.rustup.rs -sSf | sh
```

To install Rust on a Windows system, download and run [this .exe](https://www.winup.rs).

Now you will run the program using Rust's Cargo package manager (which comes installed with Rust). After changing to the project's root directory:

```bash
$ cargo run
```
